# Test Coverrage

## cài đặt trước khi chạy test
1. mở terminal tại thư mục gốc `Unit Test Introduction` và làm theo các bước sau

2.  chạy lệnh `dotnet restore` 
  - tải các package cần thiết về local

3. chạy lệnh `dotnet build`
  - build code - biên dịch code sang dll
  - kiểm tra xem có lỗi hay là không
  - project này sử dụng .net7.0 bạn sẽ phải cài .net7.0 sdk nếu như chưa có

## Run test & coverrage

1. chạy lệnh test và ghi lại kết quả

`dotnet test --collect:"XPlat Code Coverage"`
  - sau khi test run thành công kết quả sẽ ghi lại trong thư mục tên là `TestResults` nằm bên trong thư mục `UnitTestIntroductionTests` 
  - kết quả sẽ ở format xml với tên file là `coverage.cobertura.xml`
  - để nhìn kết quả trực quan hơn thì mình sẽ tạo ra report ở dạng file html bằng cách làm theo bước 2

2. cài đặt tool để tạo ra file html tự động nếu như bạn chưa cài

`dotnet tool install -g dotnet-reportgenerator-globaltool`


3. Chạy lệnh bên dưới

`reportgenerator -reports:**/coverage.cobertura.xml -targetdir:"CoverageReport" -reporttypes:"Html;JsonSummary;TextSummary"`
4. sau khi bạn chạy lệnh trên thành công thì sẽ có 1 thư mục tên là `CoverageReport` nằm trở trong root project(tức là bên trong thư mục `UnitTestIntroductionTests`)
- mở file `index.html` lên và tận hưởng kết quả